from __future__ import print_function
from __future__ import division

import os
import time
import pickle
import numpy as np
import tensorflow as tf

from keras.callbacks import EarlyStopping, ModelCheckpoint, TensorBoard

from sklearn.metrics import log_loss
from sklearn.cross_validation import LabelShuffleSplit

from utilities import write_submission, calc_geom, calc_geom_arr, mkdirp, md5
from models import vgg, resnet, nin

TESTING = os.environ.get('TESTING', False)

DATASET_PATH = os.environ.get('DATASET_PATH', 'dataset/data_10_tf.pkl' if not TESTING else 'dataset/data_10_tf_subset.pkl')
print('md5', DATASET_PATH, md5(DATASET_PATH))

CHECKPOINT_PATH = os.environ.get('CHECKPOINT_PATH', 'checkpoints/')
SUMMARY_PATH = os.environ.get('SUMMARY_PATH', 'summaries/')
MODEL_PATH = os.environ.get('MODEL_PATH', 'models/')

mkdirp(CHECKPOINT_PATH)
mkdirp(SUMMARY_PATH)
mkdirp(MODEL_PATH)

NB_EPOCHS = 100 if not TESTING else 1
MAX_FOLDS = 3 if not TESTING else 1
PATIENCE = 10

DOWNSAMPLE = 10
WIDTH, HEIGHT, NB_CHANNELS = 640 // DOWNSAMPLE, 480 // DOWNSAMPLE, 3
BATCH_SIZE = 64

with open(DATASET_PATH, 'rb') as f:
    X_train_raw, y_train_raw, X_test, X_test_ids, driver_ids = pickle.load(f)

X_train_raw = X_train_raw.astype(np.float32)
X_test = X_test.astype(np.float32)

mean_train = X_train_raw.mean()
std_train = X_train_raw.std()

mean_test = X_test.mean()
std_test = X_test.std()

X_train_raw = X_train_raw - mean_train
X_train_raw = X_train_raw / std_train

X_test = X_test - mean_test
X_test = X_test / std_test

print('train', 'mean', X_train_raw.mean(), 'std', X_train_raw.std())
print('test', 'mean', X_test.mean(), 'std', X_test.std())

_, driver_indices = np.unique(np.array(driver_ids), return_inverse=True)

predictions_total = [] # accumulated predictions from each fold
scores_total = [] # accumulated scores from each fold

model_fns = [
    ('resnet', resnet),
    ('nin', nin),
    ('vgg', vgg),
]

for model_name, model_fn in model_fns:
    num_folds = 0
    for train_index, valid_index in LabelShuffleSplit(driver_indices, n_iter=MAX_FOLDS, test_size=0.2, random_state=67):
        print('Model {}, Fold {}/{}'.format(model_name, num_folds + 1, MAX_FOLDS))

        X_train, y_train = X_train_raw[train_index,...], y_train_raw[train_index,...]
        X_valid, y_valid = X_train_raw[valid_index,...], y_train_raw[valid_index,...]

        model = model_fn(input_shape=(HEIGHT, WIDTH, NB_CHANNELS))
        model.summary()

        model_path = os.path.join(MODEL_PATH, 'model_{}_{}.json'.format(model_name, num_folds))
        with open(model_path, 'w') as f:
            f.write(model.to_json())

        summary_path = os.path.join(SUMMARY_PATH, 'model_{}_{}'.format(model_name, num_folds))
        mkdirp(summary_path)

        checkpoint_path = os.path.join(CHECKPOINT_PATH, 'model_{}_{}.h5'.format(model_name, num_folds))
        if not os.path.exists(checkpoint_path):
            callbacks = [
                EarlyStopping(monitor='val_loss', patience=PATIENCE, verbose=1, mode='auto'),
                ModelCheckpoint(checkpoint_path, monitor='val_loss', verbose=0, save_best_only=True, mode='auto'),
                TensorBoard(log_dir=summary_path, histogram_freq=0)
            ]

            model.fit(X_train, y_train, \
                      batch_size=BATCH_SIZE, nb_epoch=NB_EPOCHS, \
                      shuffle=True, \
                      verbose=1, \
                      validation_data=(X_valid, y_valid), \
                      callbacks=callbacks)
        else:
            print('Restoring model {}, fold {}/{} from checkpoint.'.format(model_name, num_folds + 1, MAX_FOLDS))
            model.load_weights(checkpoint_path)

        print('Validation...')
        predictions_valid = model.predict(X_valid, batch_size=100, verbose=1)
        score_valid = log_loss(y_valid, predictions_valid)
        scores_total.append(score_valid)

        print('Testing...')
        predictions_test = model.predict(X_test, batch_size=100, verbose=1)
        predictions_total.append(predictions_test)

        print('Model {}, Fold {}/{} Score: {}'.format(model_name, num_folds + 1, MAX_FOLDS, score_valid))

        now = int(time.time())
        submission_path = os.path.join(SUMMARY_PATH, 'submission_{}_{}_{}_{:.4}.csv' \
            .format(model_name, num_folds, now, score_valid))
        write_submission(predictions_test, X_test_ids, submission_path)

        num_folds += 1

print(scores_total)
