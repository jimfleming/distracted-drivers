from __future__ import print_function
from __future__ import division

from keras.layers import Input, merge
from keras.layers.core import Dense, Activation, Flatten, Dropout
from keras.layers.convolutional import Convolution2D, MaxPooling2D, AveragePooling2D
from keras.layers.normalization import BatchNormalization
from keras.layers.advanced_activations import LeakyReLU
from keras.models import Model
from keras.optimizers import Adam

from utilities import he_normal

def nin(input_shape):
    l = 2
    k = 30
    learning_rate = 1e-4

    x = inputs = Input(shape=input_shape)

    for i in range(l):
        filter_size = 3 if i == 0 else 2
        spatial_size = k * (i + 1)
        dropout_p = 0.8 # min(0.8, max(0.5, 1 - (1 / (i + 1))))

        x = Convolution2D(spatial_size, filter_size, filter_size, border_mode='same', init='he_normal', dim_ordering='tf')(x)
        x = LeakyReLU(alpha=0.3)(x)
        x = Dropout(dropout_p)(x)

        x = MaxPooling2D(pool_size=(2, 2), strides=(2, 2), dim_ordering='tf')(x)

        x = Convolution2D(spatial_size, 1, 1, border_mode='same', init='he_normal', dim_ordering='tf')(x)
        x = LeakyReLU(alpha=0.3)(x)

    spatial_size = k * (l + 1)
    dropout_p = 0.8

    x = Convolution2D(spatial_size, 2, 2, border_mode='same', init='he_normal', dim_ordering='tf')(x)
    x = LeakyReLU(alpha=0.3)(x)
    x = Dropout(dropout_p)(x)

    x = Convolution2D(spatial_size, 1, 1, border_mode='same', init='he_normal', dim_ordering='tf')(x)
    x = LeakyReLU(alpha=0.3)(x)

    x = Flatten()(x)

    predictions = Dense(10, activation='softmax', init=he_normal(1.0))(x)

    model = Model(input=inputs, output=predictions)
    model.compile(Adam(lr=learning_rate), loss='categorical_crossentropy', metrics=['accuracy'])
    return model

def resnet(input_shape):
    base_depth = 32
    learning_rate = 1e-4

    x = inputs = Input(shape=input_shape)

    # BLOCK 1
    x = Convolution2D(base_depth, 3, 3, border_mode='same', init='he_normal', dim_ordering='tf')(x)
    identity = Dropout(0.5)(x)

    x = BatchNormalization(axis=-1)(x)
    x = Activation('relu')(x)
    x = Dropout(0.5)(x)

    x = Convolution2D(base_depth, 3, 3, subsample=(2, 2), init='he_normal', dim_ordering='tf')(x)
    x = BatchNormalization(axis=-1)(x)

    x = merge([identity, x], mode='sum')

    x = Activation('relu')(x)
    x = Dropout(0.5)(x)

    # BLOCK 2
    x = Convolution2D(base_depth * 2, 3, 3, border_mode='same', init='he_normal', dim_ordering='tf')(x)
    identity = Dropout(0.6)(x)

    x = BatchNormalization(axis=-1)(x)
    x = Activation('relu')(x)
    x = Dropout(0.6)(x)

    x = Convolution2D(base_depth * 2, 3, 3, subsample=(2, 2), init='he_normal', dim_ordering='tf')(x)
    x = BatchNormalization(axis=-1)(x)

    x = merge([identity, x], mode='sum')

    x = Activation('relu')(x)
    x = Dropout(0.6)(x)

    # BLOCK 3
    x = Convolution2D(base_depth * 4, 3, 3, border_mode='same', border_mode='same', init='he_normal', dim_ordering='tf')(x)
    identity = Dropout(0.8)(x)

    x = BatchNormalization(axis=-1)(x)
    x = Activation('relu')(x)
    x = Dropout(0.8)(x)

    x = Convolution2D(base_depth * 4, 3, 3, subsample=(2, 2), init='he_normal', dim_ordering='tf')(x)
    x = BatchNormalization(axis=-1)(x)

    x = merge([identity, x], mode='sum')

    x = Activation('relu')(x)
    x = Dropout(0.8)(x)

    shape = x.get_shape()
    x = AveragePooling2D(pool_size=(shape[1].value, shape[2].value), strides=(1, 1), dim_ordering='tf')(x)
    x = Flatten()(x)

    predictions = Dense(10, activation='softmax', init=he_normal(1.0))(x)

    model = Model(input=inputs, output=predictions)
    model.compile(Adam(lr=learning_rate), loss='categorical_crossentropy', metrics=['accuracy'])
    return model

def vgg(input_shape):
    base_depth = 64
    learning_rate = 1e-4

    x = inputs = Input(shape=input_shape)

    # BLOCK 1
    x = Convolution2D(base_depth * 1, 3, 3, border_mode='same', init='he_normal', dim_ordering='tf')(x)
    x = BatchNormalization(axis=-1)(x)
    x = Activation('relu')(x)
    x = Dropout(0.2)(x)

    x = Convolution2D(base_depth * 1, 3, 3, border_mode='same', init='he_normal', dim_ordering='tf')(x)
    x = BatchNormalization(axis=-1)(x)
    x = Activation('relu')(x)
    x = Dropout(0.2)(x)

    x = MaxPooling2D(pool_size=(2, 2), strides=(2, 2), dim_ordering='tf')(x)

    # BLOCK 2
    x = Convolution2D(base_depth * 2, 3, 3, border_mode='same', init='he_normal', dim_ordering='tf')(x)
    x = BatchNormalization(axis=-1)(x)
    x = Activation('relu')(x)
    x = Dropout(0.4)(x)

    x = Convolution2D(base_depth * 2, 3, 3, border_mode='same', init='he_normal', dim_ordering='tf')(x)
    x = BatchNormalization(axis=-1)(x)
    x = Activation('relu')(x)
    x = Dropout(0.4)(x)

    x = MaxPooling2D(pool_size=(2, 2), strides=(2, 2), dim_ordering='tf')(x)

    # BLOCK 3
    x = Convolution2D(base_depth * 4, 3, 3, border_mode='same', init='he_normal', dim_ordering='tf')(x)
    x = BatchNormalization(axis=-1)(x)
    x = Activation('relu')(x)
    x = Dropout(0.6)(x)

    x = Convolution2D(base_depth * 4, 3, 3, border_mode='same', init='he_normal', dim_ordering='tf')(x)
    x = BatchNormalization(axis=-1)(x)
    x = Activation('relu')(x)
    x = Dropout(0.6)(x)

    x = MaxPooling2D(pool_size=(2, 2), strides=(2, 2), dim_ordering='tf')(x)

    x = Flatten()(x)

    x = Dense(base_depth * 8, activation='relu', init=he_normal(1.0))(x)
    x = Dropout(0.8)(x)

    predictions = Dense(10, activation='softmax', init=he_normal(1.0))(x)

    model = Model(input=inputs, output=predictions)
    model.compile(Adam(lr=learning_rate), loss='categorical_crossentropy', metrics=['accuracy'])
    return model
