from __future__ import print_function
from __future__ import division

import os
import math
import random
import hashlib

import numpy as np
import pandas as pd

from keras.initializations import normal, get_fans

def he_normal(gain):
    def he_normal_gain(shape, name=None, dim_ordering='tf'):
        fan_in, fan_out = get_fans(shape, dim_ordering=dim_ordering)
        s = gain * np.sqrt(1. / fan_in)
        return normal(shape, s, name=name)
    return he_normal_gain

def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

def mkdirp(path):
    try:
        os.makedirs(path)
    except OSError:
        pass

def calc_geom(scores, num_predictions):
    result = scores[0]
    for i in range(1, num_predictions):
        result *= scores[i]
    result = math.pow(result, 1.0 / num_predictions)
    return result

def calc_geom_arr(predictions_total, num_predictions):
    results = np.array(predictions_total[0])
    for i in range(1, num_predictions):
        results *= np.array(predictions_total[i])
    results = np.power(results, 1.0 / num_predictions)
    return results.tolist()

def write_submission(predictions, ids, dest):
    df = pd.DataFrame(predictions, columns=['c0', 'c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9'])
    df.insert(0, 'img', pd.Series(ids, index=df.index))
    df.to_csv(dest, index=False)
